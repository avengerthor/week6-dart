import 'package:flutter/material.dart';
import './login_page.dart';

class StopwatchScreen extends StatefulWidget {
  static const route= '/stopwatch';
  @override
  State<StatefulWidget> createState() {
    return StopwatchScreenState();
  }
}

class StopwatchScreenState extends State<StatefulWidget> {
  @override
  Widget build(BuildContext context) {
    String email = ModalRoute.of(context)!.settings.arguments as String;
    print('email=$email');
    return Scaffold(
      appBar: AppBar(title: Text(email),),
      body: Column(
        children: [
          Expanded(child: _buildCounter(context)),
          Expanded(child: _buildLapDisplay())
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.home),
        onPressed: () {
          Navigator.of(context).pushReplacementNamed(LoginScreen.route);
        },
      ),
    );
  }

  Widget _buildCounter(BuildContext context) {
    return Container(
      color: Theme.of(context).primaryColor,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text('Lap 1', style: Theme.of(context)
              .textTheme
              .subtitle1!
              .copyWith(color: Colors.white),
          ),
          Text('5 seconds', style: Theme.of(context)
              .textTheme
              .headline5!
              .copyWith(color: Colors.white)
          ),
          SizedBox(height: 20),
          _buildControls()
        ],
      ),
    );
  }

  Row _buildControls() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        ElevatedButton(
            style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(Colors.green),
                foregroundColor: MaterialStateProperty.all<Color>(Colors.white)
            ),
            onPressed: () {

            },
            child: Text('Start')
        ),
        SizedBox(width: 20),
        ElevatedButton(
            style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(Colors.yellow),
                foregroundColor: MaterialStateProperty.all<Color>(Colors.black54)
            ),
            onPressed: () {

            },
            child: Text('Lap')
        ),
        SizedBox(width: 20),
        Builder(
            builder: (context) => TextButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(Colors.red),
                foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
              ),
              child: Text('Stop'),
              onPressed: () {

              },
            )
        )
      ],
    );
  }

  Widget _buildLapDisplay() {
    return Scrollbar(
        child: ListView.builder(
            itemCount: 5,
            itemBuilder: (context, index) {
              return ListTile(
                title: Text('Lap i'),
                trailing: Text('60 seconds'),
              );
            }
        )
    );
  }
}